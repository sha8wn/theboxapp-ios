//
//  DetailViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/30/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit
import Hero

class DetailViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    var panGR = UITapGestureRecognizer()
    var heroId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.heroID = heroId
        panGR.addTarget(self, action: #selector(pan))
        view.addGestureRecognizer(panGR)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func pan() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
