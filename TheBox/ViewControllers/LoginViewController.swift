//
//  LoginViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet var signInButton: ActionButton!
    @IBOutlet var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet var appIcon: UIImageView!
    @IBOutlet var registerButton: ActionButton!
    @IBOutlet var passwordTextField: TextFieldWithImage!
    @IBOutlet var emailTextField: TextFieldWithImage!
    
    var topView = UIView()
    // MARK: - Life cyrcle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        passwordTextField.hintButtonAction = {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordViewController")
            self.navigationController?.pushViewController(controller, animated: true)
        }
        registerButton.layer.borderWidth = 0.5
        registerButton.layer.borderColor = UIColor.white.cgColor
        let center = NotificationCenter.default
        center.addObserver(self,
                           selector: #selector(keyboardWillShow(notification:)),
                           name: .UIKeyboardWillShow,
                           object: nil)
        
        center.addObserver(self,
                           selector: #selector(keyboardWillHide(notification:)),
                           name: .UIKeyboardWillHide,
                           object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        scrollView.endEditing(true)
        navigationItem.titleView = nil
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            guard topView.superview == nil else {
                return
            }
            topView = addHomeIcon()
            topView.alpha = 0
            UIView.animate(withDuration: 1, animations: {
                self.appIcon.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                self.topView.alpha = 1
            }, completion: nil)
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("Hide")
            
            UIView.animate(withDuration: 1, animations: {
                self.appIcon.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.topView.alpha = 0
            }, completion: { value in
                self.topView.removeFromSuperview()
            })
        }
    }
    
    // MARK: - IBOutlet Action
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        
        var isValid = [Bool]()
        if !emailTextField.text!.isValidEmail() {
            emailTextField.errorMessage = emailTextField.text == "" ? "Required field" : "Invalid Email"
            isValid.append(false)
        }
        isValid.append(true)
        if !passwordTextField.text!.isValidPassword() {
            passwordTextField.errorMessage = passwordTextField.text == "" ? "Required field" : "Invalid Password"
            isValid.append(false)
        }
        isValid.append(true)
        
        if !isValid.contains(false) {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerViewController")            
            present(UINavigationController(rootViewController: controller), animated: true, completion: {
                self.navigationController?.popToRootViewController(animated: true)
            })
        }
    }

    // MARK: - Validation
    
    func validate(values: [Bool]) -> Bool {
        return !values.contains(false)
    }

}

