//
//  StorageContainerViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit
import MXSegmentedPager

class StorageContainerViewController: MXSegmentedPagerController {

    //MARK: - Variables
    
    var controllers: [UIViewController] = []
    var editButtonTapped: ((String?)->Void)?
    
    //MARK: - Life cyrcle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let inStorage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InStorageViewController") as! InStorageViewController
        inStorage.editButtonTapped = editButtonTapped
        controllers.append(inStorage)
        let outOfStorage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OutOfStorageViewController") as! OutOfStorageViewController
        outOfStorage.editButtonTapped = editButtonTapped
        controllers.append(outOfStorage)
        segmentedPager.reloadData()
        segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControlEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.appGreen
        segmentedPager.parallaxHeader.height = 0
        segmentedPager.segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.formText, NSFontAttributeName : UIFont.systemFont(ofSize: 17)]
        segmentedPager.bounces = false
        segmentedPager.pager.isScrollEnabled = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SegmetnedPager Actions

    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return ["In Storage", "Out of Storage"][index]
    }
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return controllers.count
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {

        return controllers[index]
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
