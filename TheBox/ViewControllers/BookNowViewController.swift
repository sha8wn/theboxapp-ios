//
//  BookNowViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class BookNowViewController: UIViewController {

    var viewMode: FieldViewMode = .edit
    
    //MARK - IBOutlets
    @IBOutlet var button: ActionButton!
    @IBOutlet var storageStayTextField: FormTextField!
    @IBOutlet var storageSizeTextField: FormTextField!
    @IBOutlet var locationTextField: FormTextField!
    @IBOutlet var emailTextField: FormTextField!
    @IBOutlet var mobileTextField: FormTextField!
    @IBOutlet var fullnameTextField: FormTextField!
    @IBOutlet var containerView: UIView!
    @IBOutlet var animatedView: UIView!
    
    var selectedStorage: String?
    
    //MARK - Life cyrcle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addHomeIcon()
        let dummyData = ["Dummy Data1","Dummy Data2","Dummy Data3","Dummy Data4","Dummy Data5","Dummy Data6"]
        locationTextField.dataSource = dummyData
        storageSizeTextField.dataSource = ["10X5","10X10","20X10"]
        storageStayTextField.dataSource = dummyData
        configForState(mode: viewMode)
        // Do any additional setup after loading the view.
        storageSizeTextField.text = selectedStorage
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        containerView.dropShadow()
        animatedView.addCurvedLayer()
    }
    
    //MARK - Outlet Actions
    
    @IBAction func buttonTapped(_ sender: Any) {
        
        if viewMode == .edit {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookNowViewController") as! BookNowViewController
            controller.viewMode = .view
            navigationController?.pushViewController(controller, animated: true)
        } else {
            let alert = AlertViewController(nibName: "AlertView", bundle: nil)
            alert.buttonTapped = {
                self.navigationController?.popToRootViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
            alert.modalPresentationStyle = .custom
            alert.modalTransitionStyle = .crossDissolve
            present(alert, animated: true, completion: nil)
            alert.initalSetup(titleText: "Success!", messagetext: "Thank you for sending your request. We will get back to you shortly.", buttonTitle: "Ok", alertType: .success)
            
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerViewController")
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func configForState(mode: FieldViewMode) {
        
        if mode == .view {
            button.setTitle("Submit", for: .normal)
        }
    }
    
}
