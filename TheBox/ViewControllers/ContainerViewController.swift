//
//  ContainerViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit
import Hero

class ContainerViewController: UIViewController {

    //MARK: - Life cyrcle

    override func viewDidLoad() {
        super.viewDidLoad()

        addHomeIcon()
        addNavigationBarButtons()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Handle Cell edit Action

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as?  StorageContainerViewController {
            controller.editButtonTapped = { heroId in
                DispatchQueue.main.async {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListItemViewController") as! ListItemViewController
                    vc.heroId = heroId
                    self.navigationController?.isHeroEnabled = true
                    self.isHeroEnabled = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - Configure Navigation Bar

    func addNavigationBarButtons() {
        
        let leftButton = UIBarButtonItem(title: "Home", style: .plain, target: self, action: #selector(homeButtonTapped))
        leftButton.setTitleTextAttributes( [NSFontAttributeName : UIFont.systemFont(ofSize: 17.0),NSForegroundColorAttributeName : UIColor.white], for: .normal)
        navigationItem.leftBarButtonItem = leftButton
        let rightButton = UIBarButtonItem(image: UIImage(named: "signIcon")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(newButtonTapped))
        let settingsButton = UIBarButtonItem(image: UIImage(named: "settingsIcon")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(settingsButtonTapped))
        navigationItem.rightBarButtonItems = [rightButton,settingsButton]
    }
    
    func homeButtonTapped(button: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func newButtonTapped(button: UIButton) {
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListItemViewController")
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func settingsButtonTapped(button: UIButton) {
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController")
        navigationController?.pushViewController(controller, animated: true)
    }
}
