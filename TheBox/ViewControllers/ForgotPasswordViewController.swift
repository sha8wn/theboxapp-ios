//
//  ForgotPasswordViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet var appIcon: UIImageView!
    @IBOutlet var emailTextField: TextFieldWithImage!
    
    var topView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let center = NotificationCenter.default
        center.addObserver(self,
                           selector: #selector(keyboardWillShow(notification:)),
                           name: .UIKeyboardWillShow,
                           object: nil)
        
        center.addObserver(self,
                           selector: #selector(keyboardWillHide(notification:)),
                           name: .UIKeyboardWillHide,
                           object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            guard topView.superview == nil else {
                return
            }
            topView = addHomeIcon()
            topView.alpha = 0
            UIView.animate(withDuration: 1, animations: {
                self.appIcon.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                self.topView.alpha = 1
            }, completion: nil)
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("Hide")
            
            UIView.animate(withDuration: 1, animations: {
                self.appIcon.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.topView.alpha = 0
            }, completion: { value in
                self.topView.removeFromSuperview()
            })
        }
    }

    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if emailTextField.text!.isValidEmail() {
            let alert = AlertViewController(nibName: "AlertView", bundle: nil)
            alert.buttonTapped = {
                self.navigationController?.popViewController(animated: true)
            }
            alert.modalPresentationStyle = .custom
            alert.modalTransitionStyle = .crossDissolve
            present(alert, animated: true, completion: nil)
            alert.initalSetup(titleText: "Success!", messagetext: "Password Successfully Changed", buttonTitle: "Ok", alertType: .success)
        } else {
           emailTextField.errorMessage = emailTextField.text == "" ? "Required field" : "Invalid Email"
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
