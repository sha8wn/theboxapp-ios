//
//  RegisterViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    //MARK: - IBOutlets

    @IBOutlet var containerView: UIView!
    @IBOutlet var repeatPasswordTextField: FormTextField!
    @IBOutlet var passwordTextField: FormTextField!
    @IBOutlet var emailTextField: FormTextField!
    @IBOutlet var lastNameTextField: FormTextField!
    @IBOutlet var firstNameTextField: FormTextField!
    @IBOutlet var animatedView: UIView!
    
    //MARK: - Life cyrcle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addHomeIcon()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dropShadow()
        animatedView.addCurvedLayer()
    }
    
    //MARK: - Outlet Actions
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        
        var isValid = [Bool]()
        let requiredMessage = "Required field"
        if !emailTextField.text!.isValidEmail() {
            emailTextField.errorMessage = emailTextField.text == "" ? requiredMessage : "Invalid Email"
            isValid.append(false)
        }
        if !passwordTextField.text!.isValidPassword() {
            passwordTextField.errorMessage = passwordTextField.text == "" ? requiredMessage : "Invalid Password"
            isValid.append(false)
        }
        
        if firstNameTextField.text!.isEmpty {
            firstNameTextField.errorMessage = requiredMessage
            isValid.append(false)
        }
        
        if lastNameTextField.text!.isEmpty {
            lastNameTextField.errorMessage = requiredMessage
            isValid.append(false)
        }
        
        if repeatPasswordTextField.text!.isEmpty {
            repeatPasswordTextField.errorMessage = requiredMessage
            isValid.append(false)
        } else if repeatPasswordTextField.text != passwordTextField.text {
            repeatPasswordTextField.errorMessage = "Passwords not matched"
            isValid.append(false)
        }
        
        if !isValid.contains(false) {
            let alert = AlertViewController(nibName: "AlertView", bundle: nil)
            alert.buttonTapped = {
                self.navigationController?.popToRootViewController(animated: true)
            }
            alert.modalPresentationStyle = .custom
            alert.modalTransitionStyle = .crossDissolve
            present(alert, animated: true, completion: nil)
            alert.initalSetup(titleText: "Success!", messagetext: "Thank you for sending your request. We will get back to you shortly.", buttonTitle: "Ok", alertType: .success)
        } 
    }
}
