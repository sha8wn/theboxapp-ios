//
//  ListItemViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit
import ALCameraViewController
import Hero

class ListItemViewController: UIViewController {
    
    //MARK: - IBOutlets

    @IBOutlet var deleteButton: ActionButton!
    @IBOutlet var textView: FormTextView!
    @IBOutlet var userPhotoView: UIImageView!
    @IBOutlet var radionButtonsView: UIStackView!
    @IBOutlet var animatedView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var containerView: UIView!
    
    var heroId: String?
    
    //MARK: - Life cyrcle

    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = "Add item"
        var radioButton = RadioButton()
        radioButton.set(title: "Out of Storage", image: nil, borderColor: UIColor.clear.cgColor)
        radioButton.buttonTappedAction = { (button: RadioButton) in
            self.buttonTapAction(radioButton: button)
        }
        radionButtonsView.addArrangedSubview(radioButton)
        radioButton = RadioButton()
        radioButton.buttonTappedAction = { (button: RadioButton) in
            self.buttonTapAction(radioButton: button)
        }
        radioButton.set(title: "In Storage", image: UIImage(named: "radioButtonIcon"), borderColor: UIColor.appGreen.cgColor)
        radionButtonsView.addArrangedSubview(radioButton)
        addHomeIcon()
        textView.layer.borderColor = UIColor.formText.cgColor
        textView.layer.borderWidth = 0.7
        // Do any additional setup after loading the view.
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        userPhotoView.isUserInteractionEnabled = true
        userPhotoView.addGestureRecognizer(tapGestureRecognizer)
        if let id = heroId {
            userPhotoView.image = UIImage(named: "image")
            titleLabel.text = "Edit item"
            userPhotoView.heroID = id
        } else {
            deleteButton.isHidden = true
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dropShadow()
        animatedView.addCurvedLayer()
        userPhotoView.layer.cornerRadius = userPhotoView.frame.width/2
        userPhotoView.layer.masksToBounds = true
    }
    
    //MARK: - Button actions
    @IBAction func deleteButtonTapped(_ sender: Any) {
        
    }
    
    
    func buttonTapAction(radioButton: RadioButton) {
        
        if let items = radionButtonsView.arrangedSubviews as? [RadioButton] {
            for item in items {
                item.set(image: nil, borderColor: UIColor.clear.cgColor)
            }
            radioButton.set(image: UIImage(named: "radioButtonIcon"), borderColor: UIColor.appGreen.cgColor)
        }
    }

    @IBAction func saveButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK: - Camera Action
    
    func showAlert() {
        
        let alert = UIAlertController(title: nil, message: "Change profile photo", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            let cameraViewController = CameraViewController(croppingEnabled: true, allowsLibraryAccess: true) { [weak self] image, asset in
                if let iamge = image {
                    self?.userPhotoView.image = iamge
                }
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(cameraViewController, animated: true, completion: nil)
        })
        
        alert.addAction(UIAlertAction(title: "Choose from library", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            let libraryViewController = CameraViewController.imagePickerViewController(croppingEnabled: true) { [weak self] image, asset in
                if let iamge = image {
                    self?.userPhotoView.image = iamge
                }
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(libraryViewController, animated: true, completion: nil)
            
        })
        alert.addAction(UIAlertAction(title: "Remove current photo", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
            print("Photo selected")
            self.userPhotoView.image = UIImage(named: "userPlaceholder")
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func imageTapped(tapGestureRecognizer: UIGestureRecognizer) {
        showAlert()
    }

}
