//
//  MapViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 6/2/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

class MapViewController: UIViewController {

    @IBOutlet var animatedView: UIView!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addHomeIcon()
        // Do any additional setup after loading the view.
        let annotation = MKPointAnnotation()
        let centerCoordinate = CLLocationCoordinate2D(latitude: 25.136787, longitude:55.222411)
        annotation.coordinate = centerCoordinate
        annotation.title = "asdadadasd"
        mapView.addAnnotation(annotation)
        mapView.delegate = self
        mapView.centerCoordinate = centerCoordinate
        //zoomMap(byFactor: 5)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        animatedView.addCurvedLayer()
        containerView.layer.cornerRadius = 5
        containerView.layer.masksToBounds = true
        containerView.dropShadow()
    }
    
    @IBAction func whatsappButtonTapped(_ sender: Any) {
        
        let urlWhats = "whatsapp://send?text=Hi"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.openURL(whatsappURL as URL)
                } else {
                    print("please install watsapp")
                }
            }
        }
    }
    
    @IBAction func emailButtonTapped(_ sender: Any) {
        
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        sendEmail()
    }
    
    @IBAction func phoneButtonTapped(_ sender: Any) {
        callNumber(phoneNumber: "800843269")
    }
    
    private func callNumber(phoneNumber: String) {
        
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL)
            }
        }
    }
    
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["help@theboxme.com"])
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
}

extension MapViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if (annotation is MKUserLocation) {
            return nil
        }
        let reuseId = "id"
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.image = UIImage(named:"pin")
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        let span = MKCoordinateSpanMake(0.01, 0.01)
        if let corrdinate = anView?.annotation?.coordinate {
            let region = MKCoordinateRegion(center: corrdinate, span: span)
            mapView.setRegion(region, animated: true)
        }
        return anView
    }
}

extension MapViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)

    }
}
