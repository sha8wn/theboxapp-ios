//
//  HomeViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet var spashScreenImage: UIImageView!
    @IBOutlet var appIcon: UIImageView!
    @IBOutlet var animatedView: UIView!
    @IBOutlet var buttonsContainerView: UIStackView!
    
    // MARK: - Life cyrcle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        animateAppIcon()
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // MARK: - Animations
    
    func animateAppIcon() {
        
        appIcon.alpha = 0
        
        UIView.animate(withDuration: 1, delay: 0.5, animations: {
            self.appIcon.alpha = 1
        }) { (value) in
            if value {
                UIView.animate(withDuration: 0.5, animations: {
                    self.spashScreenImage.alpha = 0
                }, completion: { (value) in
                    if value {
                        self.spashScreenImage.removeFromSuperview()
                        self.animateBackgroundView()

                    }
                })
            }
        }
    }
    
    func animateBackgroundView() {
    
        let maskLayer = CAShapeLayer()
        animatedView.layer.mask = maskLayer
        // create new animation
        let anim = CABasicAnimation(keyPath: "path")
        // from value is the current mask path
        anim.fromValue = UIBezierPath(rect: animatedView.frame).cgPath
        anim.delegate = self
        // to value is the new path
        anim.toValue = animatedView.homeScreenPath().cgPath
        
        // duration of your animation
        anim.duration = 1.0
        
        // custom timing function to make it look smooth
        anim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        // add animation
        maskLayer.add(anim, forKey: nil)
        
        // update the path property on the mask layer, using a CATransaction to prevent an implicit animation
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        maskLayer.path = animatedView.homeScreenPath().cgPath
        CATransaction.commit()
    }
    
    func animateButtonsView() {
        
        buttonsContainerView.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.buttonsContainerView.alpha = 1
        })
    }
    
    // MARK: - IBOutlet Actions
    
    @IBAction func bookButtonTapped(_ sender: Any) {
        
        let alert = AlertViewController(nibName: "AlertView", bundle: nil)
        
        alert.modalPresentationStyle = .custom
        alert.modalTransitionStyle = .crossDissolve
        present(alert, animated: true, completion: nil)
        alert.initalSetup(titleText: "Success!", messagetext: "Thank you for sending your request. We will get back to you shortly.", buttonTitle: "Ok", alertType: .success)
    }
    
    @IBAction func myBoxButtonTapped(_ sender: Any) {
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func detailButtonTapped(_ sender: Any) {
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController")
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func scheduleButtonTapped(_ sender: Any) {
        
        
    }
}

extension HomeViewController: CAAnimationDelegate {

    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        if flag {
            animateButtonsView()
        }
    }
}
