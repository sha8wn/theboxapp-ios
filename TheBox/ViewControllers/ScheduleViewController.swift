//
//  ScheduleViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController {

    //MARK: - IBOutlets

    @IBOutlet var dateTextField: FormTextField!
    @IBOutlet var fullnameTextField: FormTextField!
    @IBOutlet var animatedView: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var radioButtonView: UIStackView!
    
    //MARK: - Life cyrcle

    override func viewDidLoad() {
        super.viewDidLoad()

        var radioButton = RadioButton()
        radioButton.set(title: "Yes", image: nil, borderColor: UIColor.clear.cgColor)
        radioButton.buttonTappedAction = { (button: RadioButton) in
            self.buttonTapAction(radioButton: button)
        }
        radioButtonView.addArrangedSubview(radioButton)
        radioButton = RadioButton()
        radioButton.buttonTappedAction = { (button: RadioButton) in
            self.buttonTapAction(radioButton: button)
        }
        radioButton.set(title: "No", image: UIImage(named: "radioButtonIcon"), borderColor: UIColor.appGreen.cgColor)
        radioButtonView.addArrangedSubview(radioButton)
        dateTextField.delegate = self
        addHomeIcon()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dropShadow()
        animatedView.addCurvedLayer()
    }
    
    //MARK: - Button tap Action
    
    func buttonTapAction(radioButton: RadioButton) {
        
        if let items = radioButtonView.arrangedSubviews as? [RadioButton] {
            for item in items {
                item.set(image: nil, borderColor: UIColor.clear.cgColor)
            }
            radioButton.set(image: UIImage(named: "radioButtonIcon"), borderColor: UIColor.appGreen.cgColor)
        }
    }
     
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        let alert = AlertViewController(nibName: "AlertView", bundle: nil)
        alert.buttonTapped = {
            self.navigationController?.popToRootViewController(animated: true)
        }
        alert.modalPresentationStyle = .custom
        alert.modalTransitionStyle = .crossDissolve
        present(alert, animated: true, completion: nil)
        alert.initalSetup(titleText: "Success!", messagetext: "Thank you for sending your request. We will get back to you shortly.", buttonTitle: "Ok", alertType: .success)
    }

}

//MARK: - handle Calendar View tap Action

extension ScheduleViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let calendar = TCCalendar(nibName: "TCCalendar", bundle: nil)
        calendar.selectedFromDate = Date.dateFrom(string: textField.text!)
        calendar.selectedToDate = nil
        calendar.onRangeSelected = { startDate,endDate in
            if startDate == nil {
                textField.text = ""
            } else {
                textField.text = Date.stringFrom(date: startDate ?? Date())
            }
        }
        calendar.allowsMultipleSelection = false
        calendar.modalPresentationStyle = .custom
        calendar.modalTransitionStyle = .crossDissolve
        present(calendar, animated: true, completion: nil)
        return false
    }
}
