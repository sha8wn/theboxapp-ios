//
//  ChooseStorageViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ChooseStorageViewController: UIViewController {

    //MARK: - IBOutlets

    @IBOutlet var carouselView: iCarousel!
    @IBOutlet var animatedView: UIView!
    
    //MARK: - Variables

    var data: [(title: String, dimention: String)] = []
    
    //MARK: - Life cyrcle

    override func viewDidLoad() {
        super.viewDidLoad()

        carouselView.type = .rotary
        carouselView.delegate = self
        carouselView.dataSource = self
        addHomeIcon()
        data.append((title: "Small", dimention: "10X5"))
        data.append((title: "Medium", dimention: "10X10"))
        data.append((title: "Large", dimention: "20X10"))
        carouselView.scrollToItem(at: 1, animated: true)
        carouselView.isPagingEnabled = true
        // Do any additional setup after loading the view.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        animatedView.addCurvedLayer()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Outlet Actions

    @IBAction func nextButtonTapped(_ sender: Any) {
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookNowViewController") as! BookNowViewController
        controller.viewMode = .view
        if let view = carouselView.currentItemView as? CarouselView {
            
            controller.selectedStorage = view.typeLabel.text
        }
        navigationController?.pushViewController(controller, animated: true)
        
    }
}

//MARK: - Handle Carousel View Delegate

extension ChooseStorageViewController: iCarouselDelegate {
    
    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
        return UIScreen.main.bounds.width - 80
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        if option == .angle {
            return value * 0.2
        }
        
        if option == .spacing {
            return value * 1.4
        }
        
        if option == .radius {
            return value * 4.1
        }
        
        return value
    }
}

//MARK: - Handle Carousel View DataSource

extension ChooseStorageViewController: iCarouselDataSource {
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let view: CarouselView = CarouselView.fromNib()
        view.frame.size.width = UIScreen.main.bounds.width - 70
        view.frame.size.height = carousel.frame.height - 5
        view.titleLabel.text = data[index].title
        view.typeLabel.text = data[index].dimention
        
        view.videoButtonAction = {
            
            let videoName = "The_Box_" + self.data[index].title
            
            guard let path = Bundle.main.path(forResource: videoName, ofType:"mp4") else
            {
                debugPrint("\(videoName) not found")
                return
            }
            
            let player = AVPlayer(url: URL(fileURLWithPath: path))
            let playerController = AVPlayerViewController()
            playerController.player = player
            
            self.present(playerController, animated: true)
            {
                player.play()
            }
        }
        
        return view
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
         return 3
    }
    
    
}
