//
//  OutOfStorageViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit
import Hero

class OutOfStorageViewController: UIViewController {

    //MARK - IBOutlets
    @IBOutlet var slider: UISlider!
    @IBOutlet var collectionView: UICollectionView!

    @IBOutlet var tableView: UITableView!
    
    //MARK - Variables

    var data = [1,2,3,4,5]
    var editButtonTapped: ((String?)->Void)?
    var cellSize: Float = 0

    //MARK: - Life cyrcle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCell")
        
        // Do any additional setup after loading the view.
        slider.minimumValue = Float(view.frame.width/6)
        slider.maximumValue = Float(view.frame.width/2 + 10)
        slider.value = slider.minimumValue
        cellSize = slider.value
        collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        
        if let slider = sender as? UISlider {
            let count = Int(CGFloat(view.frame.width)/CGFloat(slider.value))
            cellSize = Float(view.frame.width/CGFloat(count))
            collectionView.reloadData()
        }
    }

}

// MARK - UICollectionView Delegate

extension OutOfStorageViewController: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(cellSize)
        return CGSize(width: CGFloat(cellSize), height: CGFloat(cellSize))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ImageViewCell
        self.editButtonTapped?(cell.imageView.heroID)
    }
}

// MARK - UICollectionView DataSource

extension OutOfStorageViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageViewCell
        cell.imageView.heroID = "image\(indexPath.row + data.count)"
        cell.imageView.heroModifiers = [.fade, .scale(0.8)]
        cell.imageView.isOpaque = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}

extension OutOfStorageViewController: HeroViewControllerDelegate {
    
    func heroWillStartAnimatingTo(viewController: UIViewController) {
        
        if (viewController as? DetailViewController) != nil {
            collectionView.heroModifiers = [.cascade(delta:0.015, direction:.bottomToTop, delayMatchedViews:true)]
        } else if (viewController as? DetailViewController) != nil {
            let cell = collectionView.cellForItem(at: collectionView.indexPathsForSelectedItems!.first!)!
            collectionView.heroModifiers = [.cascade(delta: 0.015, direction: .radial(center: cell.center), delayMatchedViews: true)]
        } else {
            collectionView.heroModifiers = [.cascade(delta:0.015)]
        }
    }
    
    func heroWillStartAnimatingFrom(viewController: UIViewController) {
        
        view.heroModifiers = nil
        if (viewController as? InStorageViewController) != nil {
            collectionView.heroModifiers = [.cascade(delta:0.015), .delay(0.25)]
        } else {
            collectionView.heroModifiers = [.cascade(delta:0.015)]
        }
        if let vc = viewController as? InStorageViewController,
            let currentCellIndex = vc.collectionView?.indexPathsForVisibleItems[0],
            let targetAttribute = collectionView.layoutAttributesForItem(at: currentCellIndex) {
            collectionView.heroModifiers = [.cascade(delta:0.015, direction:.inverseRadial(center:targetAttribute.center))]
        }
    }
}
