//
//  Date.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import Foundation

extension Date {

    static func dateFrom(string: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        if string.contains("Z") {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if let date = dateFormatter.date(from: string) {
                return date
            }
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            if let date = dateFormatter.date(from: string) {
                return date
            }
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            if let date = dateFormatter.date(from: string) {
                return date
            }
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            if let date = dateFormatter.date(from: string) {
                return date
            }
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if let date = dateFormatter.date(from: string) {
                return date
            }
        }
        return nil
    }

    static func stringFrom(date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
}
