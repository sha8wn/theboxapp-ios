//
//  UIViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // MARK - Add Navigation Bar Title View
    
    @discardableResult func addHomeIcon() -> UIView {
        
        let imageView = UIImageView(image: UIImage(named: "appIcon"))
        imageView.frame = CGRect(x: 0, y: 0, width: 80, height: 20)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
        return imageView
    }
}
