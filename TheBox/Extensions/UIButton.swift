//
//  UIButton.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

extension UIButton {
    
    // MARK - Align text and Image
    
    func alignVertical(spacing: CGFloat = 6.0) {
        
        guard let imageSize = self.imageView?.image?.size,
            let text = self.titleLabel?.text,
            let font = self.titleLabel?.font
            else { return }
        let titleSize = NSString(string: text).size(attributes: [NSFontAttributeName: font])
        print(titleSize)
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(titleSize.height + spacing), right: 0.0)
        self.imageEdgeInsets = UIEdgeInsets(top: -30, left: 0.0, bottom: 0.0, right: -titleSize.width)
    }
}
