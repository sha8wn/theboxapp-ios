//
//  UIColor.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

extension UIColor {

    static var appGreen: UIColor {
        return UIColor(red: 146/255, green: 200/255, blue: 61/255, alpha: 1)
    }
    
    static var appRed: UIColor {
        return UIColor(red: 252/255, green: 62/255, blue: 51/255, alpha: 1)
    }
    
    static var formText: UIColor {
        return UIColor(red: 144/255, green: 144/255, blue: 144/255, alpha: 1)
    }
}
