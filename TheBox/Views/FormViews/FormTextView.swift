//
//  FormTextView.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

@IBDesignable
class FormTextView: UITextView {

    public var viewType = FieldViewMode.edit
    
    let border = CALayer()

    @IBInspectable var placeholder: String = "" {
        didSet {
            text = placeholder
            textColor = UIColor.lightGray
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        delegate = self
        //addFullBorder()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addBottomBorder()
    }
    
    func changeMode(viewType: FieldViewMode) {
        
        self.viewType = viewType
        if viewType == .view {
            //border.removeFromSuperlayer()
            //addBottomBorder()
        }
    }
    
    func addBottomBorder() {
        
        
    }
}

extension FormTextView: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.formText
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = UIColor.formText
        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        return true
    }
}
