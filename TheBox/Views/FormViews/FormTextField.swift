//
//  FormTextField.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

@IBDesignable
class FormTextField: TextFieldWithImage {

    @IBInspectable var rightImageName: String = "" {
        didSet {
            guard rightImageName != "" else {
                rightImageView?.removeFromSuperview()
                return
            }
            if let imageView = rightImageView {
                imageView.image = UIImage(named: rightImageName)
            } else {
                rightImageView = UIImageView()
                guard let rightImageView = rightImageView else {
                    return
                }
                pickerView = UIPickerView()
                pickerView?.delegate = self
                rightImageView.translatesAutoresizingMaskIntoConstraints = false
                rightImageView.contentMode = .center
                addSubview(rightImageView)
                addConstraint(NSLayoutConstraint(item: rightImageView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: -15.0))
                addConstraint(NSLayoutConstraint(item: rightImageView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0.0))
                addConstraint(NSLayoutConstraint(item: rightImageView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0.0))
                rightImageView.image = UIImage(named: rightImageName)
                padding.right += rightImageView.frame.width
            }
        }
    }
    
    public var dataSource: [String] = [] {
        didSet {
            pickerView?.reloadAllComponents()
        }
    }
    var itemSelectedAction: ((Int) -> Void)?
    public var viewType = FieldViewMode.edit
    public var rightImageView: UIImageView?
    public var pickerView: UIPickerView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        
        leftImageConstraint.constant = 5
        if imageName != "" {
            padding.left = 30
        }
        textColor = UIColor.formText
        selectedLineColor = UIColor.lightGray
        selectedTitleColor = UIColor.lightGray
        titleColor = UIColor.formText
        errorColor = UIColor.red
        lineColor = UIColor.lightGray
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    /*func changeMode(viewType: FieldViewMode) {
        
        self.viewType = viewType
        if viewType == .view {
            addBottomBorder()
        }
    }
    
    func addBottomBorder() {
        
        borderStyle = .none
        let border = UIView()
        addSubview(border)
        border.translatesAutoresizingMaskIntoConstraints = false
        border.backgroundColor = UIColor.lightGray
        addConstraint(NSLayoutConstraint(item: border, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: 0.0))
        addConstraint(NSLayoutConstraint(item: border, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0))
        addConstraint(NSLayoutConstraint(item: border, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0))
        addConstraint(NSLayoutConstraint(item: border, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0))
        border.addConstraint(NSLayoutConstraint(item: border, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0.0, constant: 0.7))
    }*/
}

extension FormTextField: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let _ = rightImageView {
            tintColor = UIColor.clear
        } else {
            tintColor = UIColor.blue
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if let _ = rightImageView {
            textField.inputView = pickerView
        }
    }
}

extension FormTextField: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        itemSelectedAction?(row)
        text = dataSource[row]
    }
}

extension FormTextField: UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}

enum FieldViewMode {
    
    case view
    case edit
}
