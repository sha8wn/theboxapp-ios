//
//  ImageCell.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/29/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class ImageViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: RoundedImageView!
    
}
