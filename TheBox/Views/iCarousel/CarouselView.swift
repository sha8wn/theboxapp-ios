//
//  CarouselView.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class CarouselView: UIView {

    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var button: UIButton!
    
    var videoButtonAction: (()->Void)?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        
        button.layer.borderColor = UIColor.appGreen.cgColor
        button.layer.borderWidth = 2
        button.layer.cornerRadius = 30
        button.layer.masksToBounds = true
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        dropShadow()
    }
    
    @IBAction func watchWideoButtonTapped(_ sender: Any) {
        videoButtonAction?()
    }

}
