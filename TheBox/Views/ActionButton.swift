//
//  ActionButton.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class ActionButton: UIButton {

    // MARK: - Life cyrcle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = frame.height/2
        titleLabel?.font = UIFont.systemFont(ofSize: 18)
    }

}
