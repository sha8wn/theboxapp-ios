//
//  ViewController.swift
//  JTAppleCalendar iOS Example
//
//  Created by JayT on 2016-08-10.
//
//

import JTAppleCalendar

class TCCalendar: UIViewController {

    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet var dateRangeLabel: UILabel!
    
    
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var rangeSelectedDates: [Date] = []
    
    
    // MARK: - Action Interface
    
    var cancelTapped: (() -> Void)?
    var onRangeSelected: ((_ from: Date?,_ to: Date?) -> Void)?
    
    // MARK: - Config
    
    var generateInDates: InDateCellGeneration = .forAllMonths
    var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    let firstDayOfWeek: DaysOfWeek = .monday
    var hasStrictBoundaries = true
    
    var calendarStartDate: Date?
    var calendarEndDate: Date?
    
    var selectedFromDate: Date?
    var selectedToDate: Date?
    var allowsMultipleSelection: Bool = false
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendarView.calendarDataSource = self
        calendarView.calendarDelegate = self
        calendarView.minimumLineSpacing = 4
        calendarView.minimumInteritemSpacing = -0.5
        
        calendarView.allowsMultipleSelection = true
        calendarView.isRangeSelectionUsed = allowsMultipleSelection
        calendarView.scrollToDate(Date(), triggerScrollToDateDelegate: true, animateScroll: false,
                                  preferredScrollPosition: nil, extraAddedOffset: 0, completionHandler: nil)
        
        calendarView.register(UINib(nibName: "CalendarDayCell", bundle: Bundle.main), forCellWithReuseIdentifier: "CalendarDayCellId")
        
        let panGensture = UILongPressGestureRecognizer(target: self, action: #selector(didStartRangeSelecting(gesture:)))
        panGensture.minimumPressDuration = 0.5
        calendarView.addGestureRecognizer(panGensture)
        
        if let fromDate = selectedFromDate {
            if let toDate = selectedToDate {
                calendarView.selectDates(from: fromDate, to: toDate)
            } else {
                calendarView.selectDates([fromDate])
            }
        }
        
        showMonthNameFor(date: Date())
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let firstDateInfo = calendarView.visibleDates().indates.first {
            calendarView.viewWillTransition(to: size, with: coordinator, focusDateIndexPathAfterRotate: firstDateInfo.indexPath)
        } else if let firstDateInfo = calendarView.visibleDates().monthDates.first {
            calendarView.viewWillTransition(to: size, with: coordinator, focusDateIndexPathAfterRotate: firstDateInfo.indexPath)
        }
    }
    
    // MARK: - Methods
    
    func didStartRangeSelecting(gesture: UILongPressGestureRecognizer) {
        guard let gestureView = gesture.view else { return }
        
        let point = gesture.location(in: gestureView)
        rangeSelectedDates = calendarView.selectedDates
        
        if let cellState = calendarView.cellStatus(at: point) {
            let date = cellState.date
            if !calendarView.selectedDates.contains(date) {
                let dateRange = calendarView.generateDateRange(from: calendarView.selectedDates.first ?? date, to: date)
                for aDate in dateRange {
                    if !rangeSelectedDates.contains(aDate) {
                        rangeSelectedDates.append(aDate)
                    }
                }
                if let firstDate = rangeSelectedDates.first {
                    calendarView.selectDates(from: firstDate, to: date, keepSelectionIfMultiSelectionAllowed: true)
                }
            } else {
                guard let indexOfLastlySelectedDate = rangeSelectedDates.index(of: date),
                    let followingDay = Calendar.current.date(byAdding: .day, value: 1, to: date),
                    let rangeLastDate = rangeSelectedDates.last
                    else { return }
                
                let lastIndex = rangeSelectedDates.endIndex
                calendarView.selectDates(from: followingDay, to: rangeLastDate, keepSelectionIfMultiSelectionAllowed: false)
                rangeSelectedDates.removeSubrange(indexOfLastlySelectedDate + 1..<lastIndex)
            }
        }
        
        if gesture.state == .ended {
            rangeSelectedDates.removeAll()
        }
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        showMonthNameFor(date: visibleDates.monthDates.first?.date)
    }
    
    func showMonthNameFor(date: Date?) {
        guard let startDate = date, let month = Calendar.current.dateComponents([.month], from: startDate).month else {
            return
        }
        let monthName = DateFormatter().monthSymbols[(month - 1) % 12]
        let year = Calendar.current.component(.year, from: startDate)
        dateRangeLabel.text = monthName + " " + year.description
    }
    
    @IBAction func okButtonTapped(_ sender: UIButton) {
        
        if calendarView.selectedDates.count == 1, let fromDate = calendarView.selectedDates.first {
            onRangeSelected?(fromDate, nil)
        } else if calendarView.selectedDates.count > 1, let fromDate = calendarView.selectedDates.first, let toDate = calendarView.selectedDates.last {
            onRangeSelected?(fromDate, toDate)
        } else {
            onRangeSelected?(nil,nil)
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
         dismiss(animated: true, completion: nil)
    }
    
    @IBAction func prevButtonTapped(_ sender: UIButton) {
        calendarView.scrollToSegment(.previous)
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        calendarView.scrollToSegment(.next)
    }
    
    // MARK: - Selection Handlers
    
    func handleCellConfiguration(cell: JTAppleCell?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? CalendarDayCell else {
            return
        }
        
        if cellState.isSelected {
            myCustomCell.dayLabel.textColor = UIColor.white
        } else {
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.dayLabel.textColor = UIColor.black
            } else {
                myCustomCell.dayLabel.textColor = UIColor.gray
            }
        }
    }
    
    func handleCellSelection(view: JTAppleCell?, cellState: CellState) {
        guard let cell = view as? CalendarDayCell else { return }
        let date = cellState.date
      
        if cellState.isSelected {
            if calendarView.selectedDates.contains(date.nextDay) && calendarView.selectedDates.contains(date.previousDay) {
                cell.selectionStyle = date.isMonday ? .left :  date.isSunday ? .right : .middle
            } else if calendarView.selectedDates.contains(date.nextDay) && !calendarView.selectedDates.contains(date.previousDay) {
                cell.selectionStyle = date.isSunday ? .single : .left
            } else if !calendarView.selectedDates.contains(date.nextDay) && calendarView.selectedDates.contains(date.previousDay) {
                cell.selectionStyle = date.isMonday ? .single : .right
            } else {
                cell.selectionStyle = .single
            }
        } else {
            cell.selectionStyle = .none
        }
        
        cell.dayLabel.layer.borderWidth = 1
        cell.dayLabel.layer.cornerRadius = cell.contentView.bounds.height / 2
        cell.dayLabel.layer.masksToBounds = false
        cell.dayLabel.layer.borderColor = NSCalendar.current.isDateInToday(date) ?  UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 226.0/255.0, alpha: 1).cgColor : UIColor.clear.cgColor

    }
}

extension TCCalendar: JTAppleCalendarViewDelegate {

    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
        guard calendar.selectedDates.count > 1 else {
          return
        }
        calendar.deselectAllDates(triggerSelectionDelegate: false)
        calendar.selectDates([date], triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: true)
        
    }

    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard date.withoutTime >= Date().withoutTime else {
            return
        }
        handleCellConfiguration(cell: cell, cellState: cellState)
        guard let from = calendar.selectedDates.first, let to = calendar.selectedDates.last else {
            return
        }
        calendar.selectDates(from: from, to: to, triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: true)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell, cellState: CellState) -> Bool {
        guard date.withoutTime >= Date().withoutTime else {
            if NSCalendar.current.isDateInToday(date) {
                return true
            }
            return false
        }
        if !allowsMultipleSelection {
            calendar.deselectAllDates()
        }
        return true
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates)
    }
    
    func scrollDidEndDecelerating(for calendar: JTAppleCalendarView) {
        let visibleDates = calendarView.visibleDates()
        self.setupViewsOfCalendar(from: visibleDates)
    }
}

extension TCCalendar: JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        let startDate = Date()
        let endDate = calendarEndDate ?? (formatter.date(from: "2068-02-01") ?? Date())
        let numberOfRows = 6
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: numberOfRows,
                                                 calendar: Calendar.current,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: firstDayOfWeek,
                                                 hasStrictBoundaries: hasStrictBoundaries)
        return parameters
    }

    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let calendarDayCell = calendar.dequeueReusableCell(withReuseIdentifier: "CalendarDayCellId", for: indexPath) as! CalendarDayCell
        
        calendarDayCell.dayLabel.text = cellState.text
        
        handleCellConfiguration(cell: calendarDayCell, cellState: cellState)
        return calendarDayCell
    }
}

extension Date {
    
    var nextDay: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    
    var previousDay: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    
    var isMonday: Bool {
        return  Calendar.current.component(.weekday, from: self) == 2
    }

    var isSunday: Bool {
        return  Calendar.current.component(.weekday, from: self) == 1
    }
    
    var withoutTime: Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: dateFormatter.string(from: self)) {
            return date
        }
        return self
    }

}
