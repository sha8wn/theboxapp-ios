//
//  CalendarDayCell.swift
//  Custom Controls
//
//

import JTAppleCalendar

enum SelectionType {
    case single
    case left
    case right
    case middle
    case none
}

class CalendarDayCell: JTAppleCell {
    
    @IBOutlet var selectedView: AnimationView!
    @IBOutlet var dayLabel: UILabel!
    
    var shapeLayer = CAShapeLayer()
    
    var selectionStyle: SelectionType = .none {
        didSet {
            layoutIfNeeded()
            configureSelectedView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shapeLayer.frame = selectedView.bounds
        selectedView.layer.mask = shapeLayer
    }
    
    func configureSelectedView() {
        shapeLayer.masksToBounds = true
        switch selectionStyle {
        case .none:
            selectedView.isHidden = true
        case .single:
            selectedView.isHidden = false
            let rect = CGRect(x: ((bounds.width - selectedView.bounds.height) / 2) + 1, y: 1, width: selectedView.bounds.height - 2, height: selectedView.bounds.height - 2)
            shapeLayer.path = UIBezierPath(roundedRect:  rect, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: rect.width / 2, height: rect.height / 2)).cgPath
        case .left:
            selectedView.isHidden = false
            let rect = CGRect(x: 1, y: 1, width: selectedView.bounds.width , height: selectedView.bounds.height - 2)
            shapeLayer.path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.bottomLeft, .topLeft],
                                           cornerRadii: CGSize(width: rect.width / 2, height: rect.height / 2)).cgPath
        case .right:
            selectedView.isHidden = false
            let rect = CGRect(x: -1, y: 1, width: selectedView.bounds.width , height: selectedView.bounds.height - 2)
            shapeLayer.path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.bottomRight, .topRight],
                                           cornerRadii: CGSize(width: rect.width / 2, height: rect.height / 2)).cgPath
        case .middle:
            selectedView.isHidden = false
            let rect = CGRect(x: 0, y: 1, width: selectedView.bounds.width, height: selectedView.bounds.height - 2)
            shapeLayer.path = UIBezierPath(roundedRect:  rect, byRoundingCorners: [.allCorners],
                                           cornerRadii: CGSize(width: 0, height: 0)).cgPath
            shapeLayer.masksToBounds = false
        }
        setNeedsLayout()
    }
}
