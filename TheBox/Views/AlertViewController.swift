//
//  AlertViewController.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet var button: ActionButton!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bluredView: UIView!

    var buttonTapped: (()->Void)?
    // MARK: - Life cyrcle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    // MARK: - Setup view with data
    
    func initalSetup(titleText: String?,messagetext: String?, buttonTitle: String?, alertType: AlertType) {
        
        if let title = titleText {
            titleLabel.text = title.uppercased()
        } else {
            titleLabel.isHidden = true
        }
        if let message = messagetext {
            messageLabel.text = message
        } else {
            messageLabel.isHidden = true
        }
        if let buttonTitle = buttonTitle {
            button.setTitle(buttonTitle, for: .normal)
        } else {
            button.isHidden = true
        }
        
        if alertType == .success {
            button.backgroundColor = UIColor.appGreen
            titleLabel.textColor = UIColor.appGreen
        } else {
            button.backgroundColor = UIColor.appRed
            titleLabel.textColor = UIColor.appRed
        }
    }
    
    // MARK: - Handle Button Callback
    
    @IBAction func buttonTapped(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.buttonTapped?()
        })
    }
}

// MARK: - AlertView type

enum AlertType {
    
    case success
    case failure
}
