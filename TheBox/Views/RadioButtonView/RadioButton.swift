//
//  RadioButton.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/19/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class RadioButton: UIButton {

    var buttonTappedAction: ((RadioButton) -> Void)?
    
    func set(title: String, image: UIImage?, borderColor: CGColor) {
        
        set(image: image,borderColor: borderColor)
        setTitle(title, for: .normal)
        addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }
    
    func set(image: UIImage?, borderColor: CGColor) {
        
        if let image = image {
            setImage(image, for: .normal)
            titleEdgeInsets = UIEdgeInsets(top: 0, left:  5, bottom: 0, right: 0)
            setTitleColor(UIColor.black, for: .normal)
        } else {
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            setImage(nil, for: .normal)
            setTitleColor(UIColor.formText, for: .normal)
        }
        layer.borderWidth = 1
        layer.borderColor = borderColor
        titleLabel?.font = UIFont.systemFont(ofSize: 15)
        titleLabel?.numberOfLines = 0
        titleLabel?.lineBreakMode = .byWordWrapping
        titleLabel?.textAlignment = .center
        
    }
    
    func buttonTapped(button: RadioButton) {
        buttonTappedAction?(button)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = frame.height/2
    }
}
