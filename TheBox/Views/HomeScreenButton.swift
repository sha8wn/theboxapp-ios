//
//  HomeScreenButtons.swift
//  TheBox
//
//  Created by Narek Simonyan on 5/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit

class HomeScreenButton: UIButton {

     // MARK: - Life cyrcle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        alignVertical(spacing: 13)
        layer.cornerRadius = 5
        layer.masksToBounds = true
        titleLabel?.numberOfLines = 0
        titleLabel?.textAlignment = .center
        setTitleColor(UIColor(red: 103.0/255.0, green: 103.0/255.0, blue: 103.0/255.0, alpha: 1.0), for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        dropShadow()
    }
}



