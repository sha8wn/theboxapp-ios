//
//  TextFieldWithImage.swift
//  TradeCloud
//
//  Created by Narek Simonyan on 3/18/17.
//  Copyright © 2017 Narek Simonyan. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

@IBDesignable
class TextFieldWithImage: SkyFloatingLabelTextField {

    // MARK: - IBInspectable variables
    
    @IBInspectable public var imageName: String = ""
    @IBInspectable public var hintImageName: String = ""

    // MARK: - hintButton tap callback
    
    var hintButtonAction: (()->Void)?
    
    // MARK: - private views
    
    private var imageView: UIImageView!
    private var hintButton: UIButton!
    var leftImageConstraint = NSLayoutConstraint()
    var padding = UIEdgeInsets(top: 13, left: 35, bottom: 0, right: 5)
    
    // MARK: - Life cyrcle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        textColor = UIColor.white
        if imageName != "" {
            imageView = UIImageView()
            imageView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(imageView)
            leftImageConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: 10.0)
            addConstraint(leftImageConstraint)
            addConstraint(NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0.0))
            addConstraint(NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 10.0))
            imageView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0.0, constant: 15))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage(named: imageName)
        }else {
            padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        if hintImageName != "" {
            
            hintButton = UIButton()
            hintButton.translatesAutoresizingMaskIntoConstraints = false
            addSubview(hintButton)
            addConstraint(NSLayoutConstraint(item: hintButton, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: 0.0))
            addConstraint(NSLayoutConstraint(item: hintButton, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0.0))
            addConstraint(NSLayoutConstraint(item: hintButton, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 15.0))

            hintButton.setImage(UIImage(named: hintImageName), for: .normal)
            hintButton.addTarget(self, action: #selector(hintButtonTapped), for: .touchUpInside)
            padding.right = 20
        }
        selectedLineColor = UIColor.white
        selectedTitleColor = UIColor.white
        titleColor = UIColor.white
        errorColor = UIColor.red
        lineColor = UIColor.white
        lineHeight = 1
        titleFormatter = {str in
            return str
        }
        addTarget(self, action: #selector(valueChanged), for: .editingChanged)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        
        return UIEdgeInsetsInsetRect(bounds, padding)
    }

    override func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {
        
        let titleHeight = self.titleHeight()
        let xMargin:CGFloat = imageView == nil ? 0 : 10
        if editing {
            return CGRect(x: xMargin, y: -5, width: bounds.size.width, height: 20)
        }
        return CGRect(x: xMargin, y: titleHeight, width: bounds.size.width, height: 20)
    }
    
    // MARK: - UIView Actions
    
    func hintButtonTapped(button: UIButton) {
        hintButtonAction?()
    }
    
    func valueChanged(textField: UITextField) {
        errorMessage = nil
    }
}
